
Inleiding Wetenschappelijk Onderzoek (Introduction to Research Methods) - Final Project

Name: Thijmen M. Dam
Student Number: S3360695
Mail: T.M.Dam@student.rug.nl

Please clone this repository to any location on a LWP machine on the Rijksuniversiteit Groningen (RUG).


freq.sh
----
This bash script presents you with the frequency of appearance of the Dutch political parties VVD, PVV, CDA and D66
in the Twitter Corpus of the Rijksuniversiteit Groningen. It is separated in two periods of seven days:
Period 1 (March 8 - 14) and period 2 (March 16-22). Retweets are disregarded and excluded.
---

tweet_amount.sh
---
This bash script presents you with the amount of tweets in the Twitter Corpus of the Rijksuniversiteit
Groningen, separated in two periods of seven days: Period 1 (March 8 - 14) and period 2 (March 16-22).
Retweets are disregarded and excluded.
---


To run a script, make sure you cloned this repository to a LWP machine of the RUG.
Open terminal, change directory to the cloned repository ('cd iwo-source') and run either
'bash freq.sh' or 'bash tweet_amount.sh' depending on which script you would like to run.