#!/usr/bin/env bash

# This bash script presents you with the amount of tweets in the Twitter Corpus of the Rijksuniversiteit
# Groningen, separated in two periods of seven days: Period 1 (March 8 - 14) and period 2 (March 16-22).
# Retweets are disregarded and excluded.

# Counting tweets in period 1
total_p=$(gunzip -c /net/corpora/twitter2/Tweets/2017/03/{20170308..20170314}*.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text \
| grep -v '^RT' | wc -l)

# Counting tweets in period 2
total_a=$(gunzip -c /net/corpora/twitter2/Tweets/2017/03/{20170316..20170322}*.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text \
| grep -v '^RT' | wc -l)

# Printing the amount of tweets of period 1
echo "March 8 <-> 14"
echo "Total     $total_p"

# Printing the amount of tweets of period 2
echo "March 16 <-> 22"
echo "Total     $total_a"