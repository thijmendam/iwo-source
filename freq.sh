#!/usr/bin/env bash

# This bash script presents you with the frequency of appearance of the Dutch political parties VVD, PVV, CDA and D66
# in the Twitter Corpus of the Rijksuniversiteit Groningen. It is separated in two periods of seven days:
# Period 1 (March 8 - 14) and period 2 (March 16-22). Retweets are disregarded and excluded.

# Tweets in period 1 containing VVD, PVV, CDA or D66.
tweets_prior=$(gunzip -c /net/corpora/twitter2/Tweets/2017/03/{20170308..20170314}*.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text \
| grep -v '^RT' | grep -Eo '\w+' | grep -Eoi 'VVD|PVV|CDA|D66')

# Tweets in period 2 containing VVD, PVV, CDA or D66.
tweets_after=$(gunzip -c /net/corpora/twitter2/Tweets/2017/03/{20170316..20170322}*.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text \
| grep -v '^RT' | grep -Eo '\w+' | grep -Eoi 'VVD|PVV|CDA|D66')

# Acquiring the frequencies of period 1.
VVD_p=$(echo "$tweets_prior" | grep -i 'VVD' | wc -l)
PVV_p=$(echo "$tweets_prior" | grep -i 'PVV' | wc -l)
CDA_p=$(echo "$tweets_prior" | grep -i 'CDA' | wc -l)
D66_p=$(echo "$tweets_prior" | grep -i 'D66' | wc -l)
total_p=$(echo "$tweets_prior" | wc -l)

# Acquiring the frequencies of period 2.
VVD_a=$(echo "$tweets_after" | grep -i 'VVD' | wc -l)
PVV_a=$(echo "$tweets_after" | grep -i 'PVV' | wc -l)
CDA_a=$(echo "$tweets_after" | grep -i 'CDA' | wc -l)
D66_a=$(echo "$tweets_after" | grep -i 'D66' | wc -l)
total_a=$(echo "$tweets_after" | wc -l)

# Printing the frequencies of period 1.
echo "March 8 <-> 14"
echo "Party     Frequency"
echo "VVD       $VVD_p"
echo "PVV       $PVV_p"
echo "CDA       $CDA_p"
echo "D66       $D66_p"
echo "Total     $total_p"

# Printing the frequencies of period 2.
echo "March 16 <-> 22"
echo "Party     Frequency"
echo "VVD       $VVD_a"
echo "PVV       $PVV_a"
echo "CDA       $CDA_a"
echo "D66       $D66_a"
echo "Total     $total_a"